package um.fds.agl.ter23.repositories;

import org.springframework.data.repository.CrudRepository;

import um.fds.agl.ter23.entities.Groupe;

public interface GroupeRepository extends CrudRepository<Groupe, Long> {

}
